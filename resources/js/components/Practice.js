/* eslint-disable import/no-extraneous-dependencies */
import React from "react";
import "fullpage.js/vendors/scrolloverflow"; // Optional. When using scrollOverflow:true
import ReactFullpage from "@fullpage/react-fullpage";


// NOTE: if using fullpage extensions/plugins put them here and pass it as props
const pluginWrapper = () => {
    /**
     * require('fullpage.js/vendors/scrolloverflow'); // Optional. When using scrollOverflow:true
     */
};

const originalColors = ['#282c34', '#ff5f45', '#0798ec'];

export default class Practice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sectionsColor: [...originalColors],
            fullpages: [
                {
                    text: 'section 1',
                },
                {
                    text: 'section 2',
                },
            ],
        };
    }

    onLeave(origin, destination, direction) {
        console.log('onLeave', { origin, destination, direction });
        // arguments are mapped in order of fullpage.js callback arguments do something
        // with the event
    }

    handleChangeColors() {
        const newColors =
            this.state.sectionsColor[0] === 'yellow'
                ? [...originalColors]
                : ['yellow', 'blue', 'white'];
        this.setState({
            sectionsColor: newColors,
        });
    }

    handleAddSection() {
        this.setState(state => {
            const { fullpages } = state;
            const { length } = fullpages;
            fullpages.push({
                text: `section ${length + 1}`,
                id: Math.random(),
            });

            return {
                fullpages: [...fullpages],
            };
        });
    }

    handleRemoveSection() {
        this.setState(state => {
            const { fullpages } = state;
            const newPages = [...fullpages];
            newPages.pop();
            return { fullpages: newPages };
        });
    }

    render() {
        const { fullpages } = this.state;

        if (!fullpages.length) {
            return null;
        }

        const Menu = () => (
            <div
                className="menu"
                style={{
                    position: 'fixed',
                    top: '70px',
                    zIndex: 100,
                }}
            >
                <ul>
                    <li>
                        <button onClick={() => this.handleAddSection()}>+ Section</button>
                        <button onClick={() => this.handleRemoveSection()}>
                            - Section
                        </button>
                        <button onClick={() => this.handleChangeColors()}>
                            Change colors
              </button>
                    </li>
                </ul>
            </div>
        );
        const Button = () => (
            
            <div
                className="button"
                style={{
                    position: 'fixed',
                    top: '100px',
                    zIndex: 100,
                }}
            >
                <button onClick={this.scrollToMyRef}>
                    Click me to move down
                </button>
            </div>
        );

        return (
            <div className="App">
                <Menu />
                <Button />
                <ReactFullpage
                    debug /* Debug logging */
                    scrollOverflow
                    navigation
                    onLeave={this.onLeave.bind(this)}
                    sectionsColor={this.state.sectionsColor}
                    pluginWrapper={pluginWrapper}
                    render={({ state, fullpageApi }) => (                        
                        <ReactFullpage.Wrapper>
                            {/* {fullpages.map(({ text }) => (
                                <div key={text} className="section">
                                    <h1>{text}</h1>
                                </div>
                            ))} */}

                            <div className="section">
                                <h1>section1</h1>
                            </div>

                            <div className="section">
                                <button onClick={() => fullpageApi.moveSectionUp()} 
                                    style={{
                                    position: 'relative',
                                    top: '200px',
                                    zIndex: 100,    
                                }}>
                                    Click me to move carousel
                                </button>
                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img class="d-block w-100" src="..." alt="First slide" />
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block w-100" src="..." alt="Second slide" />
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block w-100" src="..." alt="Third slide" />
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                            <div className="section" ref={this.myRef}>
                                <button onClick={() => fullpageApi.moveSectionUp()}>
                                    Click me to move down
                                </button>
                                <h1>section2</h1>
                            </div>
                        </ReactFullpage.Wrapper>
                    )}
                />
            </div>
        );
    }
}

