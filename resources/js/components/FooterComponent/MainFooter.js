import React, { Component } from 'react';
import { FacebookLoginButton, GoogleLoginButton, TwitterLoginButton, InstagramLoginButton } from "react-social-login-buttons";
import { SocialIcon } from 'react-social-icons';
import Icon from 'react-geomicons';
import { Twitter, Location, Telephone, Message, Mail } from 'react-bytesize-icons';
import MaterialIcon, { colorPalette } from 'material-icons-react';
import MapContainer from '../Head/MapComponent/MapContainer.js';
import MessageForm from '../Head/MessageForm/MessageForm';
import '../../../css/header/home.css';


export default class MainFooter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hrefGoogle: 'http://www.google.com',
            hrefFacebook: 'http://www.facebook.com',
            hreTwitter: 'http://www.twitter.com',
            hrefInstagram: 'http://www.instagram.com',
            leftBrace: '{',
            rightBrace: '}',
            copyright_text_sts: '2019, All Rights Are Reserved By Sustainable Technological Solutions.',
            contact_our_sts_team: [
                { full_name: 'name', number: 989384 },
                { full_name: 'name1', number: 9403943 },
                { full_name: 'name2', number: 9438948 },
            ],
            stsdetail: [],
            phone_number_sts:'',
            mail_id_sts: '',
            location_sts: '',
        }
    }

    componentWillMount() {
        axios.get('/api/stsdetail').then(response => {
            this.setState({
                stsdetail: response.data
            })
        }).catch(errors => {
            console.log(errors);
        })         
    }
    componentDidMount() {
        setTimeout(()=>{
            this.state.stsdetail.map(sts=>{
                if(sts.item==='phone'){
                    this.setState({
                        phone_number_sts: sts.content
                    })
                }
                if(sts.item==='email'){
                    this.setState({
                        mail_id_sts: sts.content
                    })
                }
                if(sts.item==='location'){
                    this.setState({
                        location_sts: sts.content
                    })
                }
            })
        },2000)
    }
    render() {
        
        const contact_our_sts_team = this.state.contact_our_sts_team;

        const render_contact_our_sts_team_list = contact_our_sts_team.map(cost => {
            return (
                <div key={cost.number} style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    <div>{this.state.leftBrace + 'Er. ' + cost.full_name + this.state.rightBrace}</div>
                    <div style={{ color: 'yellowgreen' }}>{cost.number}</div>
                </div>
            )
        })

        return (
            <div className="fullPageSectionFourth" >
                {/* <div style={{height:'40px'}}>hello</div> */}
                <div className="sectionFourthContainer">
                    <div className="sectionFourthFirstCol">
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                            <img src={require('../../../assest/icon/stsLogo.png')} style={{ width: '65px', height: '65px' }}></img>
                            <div style={{ fontSize: '17px', width: '100%', textAlign: 'center' }}>Sustainable Technological Solutions</div>
                            <div className="imageBorder" ></div>
                        </div>
                        <div style={{ height: '250px', display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly' }}>
                            <div style={{ display: 'flex', flexDirection: 'row', marginTop: '10px' }}>
                                <Telephone></Telephone>
                                <div style={{ marginLeft: '12px' }}>{this.state.phone_number_sts}</div>
                            </div>
                            <div style={{ display: 'flex', flexDirection: 'row' }}>
                                <Mail></Mail>
                                <div style={{ marginLeft: '12px' }}>{this.state.mail_id_sts}</div>
                            </div>
                            <div style={{ display: 'flex', flexDirection: 'row' }}>
                                <Location></Location>
                                <div style={{ marginLeft: '12px' }}>{this.state.location_sts} </div>
                            </div>
                        </div>

                    </div>
                    <div className="sectionFourthFirstCol">
                        {/* <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                            <MaterialIcon icon="perm_phone_msg" size="large" color="white"></MaterialIcon>
                            <div style={{ fontSize: '17px' }}>Contact Our STS Team</div>
                            <div className="imageBorder" ></div>
                        </div> */}
                        <div style={{ height: '250px', display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly',position:'absolute' }}>
                            {/* <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <div>{this.state.leftBrace + 'Er. Name' + this.state.rightBrace}</div>
                                <div style={{ color: 'yellowgreen' }}>+977-9843563422</div>
                            </div>
                            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <div>{this.state.leftBrace + 'Er. Name' + this.state.rightBrace}</div>
                                <div style={{ color: 'yellowgreen' }} >+977-9843563422</div>
                            </div>
                            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <div>{this.state.leftBrace + 'Er. Name' + this.state.rightBrace}</div>
                                <div style={{ color: 'yellowgreen' }}>+977-9843563422</div>
                            </div> */}
                            {/* {render_contact_our_sts_team_list} */}
                            <MessageForm/>
                        </div>

                    </div>
                    <div className="sectionFourthFirstCol last-col-map">
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                            <MaterialIcon icon="location_on" size="large" color="white"></MaterialIcon>
                            <div style={{ fontSize: '17px' }}>Location Map</div>
                            <div className="imageBorder" ></div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'row', background: 'black', width: '100%', height: '250px', position: 'relative' }}>
                            <div style={{ width: '100%', height: '100%', position: 'absolute', borderStyle: 'solid', borderColor: '#9CF5A6', borderWidth: '1px' }} >
                                <MapContainer />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <div className="textFooter">
                        {this.state.copyright_text_sts}
                    </div>
                    <div className="iconFooter">
                        {/* <Twitter className="icon"></Twitter>
                                        <Icon className="icon" name="heart"></Icon>
                                        <Icon className="icon" name="heart"></Icon>
                                        <Icon className="icon" name="heart"></Icon> */}
                        <a href={this.state.hrefFacebook} target="_blank">
                            <SocialIcon className="icon" network="facebook" style={{ width: '30px', height: '30px', color: 'white' }}></SocialIcon>
                        </a>
                        <a href={this.state.hrefGoogle} target="_blank">
                            <SocialIcon className="icon" network="google" style={{ width: '30px', height: '30px' }}></SocialIcon>
                        </a>
                        <a href={this.state.hreTwitter} target="_blank">
                            <SocialIcon className="icon" network="twitter" style={{ width: '30px', height: '30px' }}></SocialIcon>
                        </a>
                        <a href={this.state.hrefInstagram} target="_blank">
                            <SocialIcon className="icon" network="instagram" style={{ width: '30px', height: '30px' }}></SocialIcon>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}