import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import BannerAnim from 'rc-banner-anim';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import './index.css';
import './thumb.css';
import '../index.css';
import '../../../css/header/home.css';

import mobile from '../../../assest/image/mobileApplication.jpg';
import web from '../../../assest/image/webApplication.jpg';

const { Element, Thumb } = BannerAnim;
const BgElement = Element.BgElement;
export default class HomeFeature extends Component {
    constructor(props) {
        super(props);
        this.imgArray = [
            mobile,
            web
        ];

        this.state = {
            enter: true,
            arrowShow: false,
            autoPlay: false,
            autoPlaySpeed: 5000,
            dragPlay: false,
            isLoaded:false,
            data:[],
            serviceContent: 'Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.',

        };
    }
    componentDidMount(){
        axios.get('api/feature')
        .then(res=>{
            this.setState({
                data:res.data,
                isLoaded:true
            });
            console.log(this.state.data);
        })
    }
    onMouseEnter() {
        this.setState({
            enter: true,
        });
    }

    onMouseLeave() {
        this.setState({
            enter: true,
        });
    }

    render() {
        const thumbChildren = this.imgArray.map((img, i) =>
            <span className="thumbnail-services" key={i}><i style={{ backgroundImage: `url(${img})` }} /></span>
        );
        var {isLoaded,data} = this.state;
        if(isLoaded){
            return (
                <BannerAnim onMouseEnter={this.onMouseEnter.bind(this)}
                    onMouseLeave={this.onMouseLeave.bind(this)}
                    arrow={this.state.arrowShow}
                    autoPlay={this.state.autoPlay}
                    dragPlay={this.state.dragPlay}
                    autoPlaySpeed={this.state.autoPlaySpeed}>
                    {data.map(each=>(
                    <Element key={'element'+each.id}
                    prefixCls="banner-user-elem"
                >
                    <BgElement
                        key={'bgelelement'+each.id}
                        className="bg"
                        style={{
                            backgroundImage: `linear-gradient(
                                rgba(0, 100, 50, 0.5),
                                rgba(0, 100, 50, 0.5)
                              ),url(${this.imgArray[each.id-1]})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                        }}
                    />
                    <QueueAnim key={'queueanim'+each.id} name="QueueAnim" >
                        <div style={{
                            marginTop: '70px',
                            width: 'inherit',
                            display: 'flex',
                            color:'#FFFFFF',
                            background: 'transparent',
                            flexDirection: 'column',
                            justifyContent: 'justify',
                            alignItems: 'center'
                        }}>
                            <h1 key="h1" className="title-content">Our Feature</h1>
                            <div className="contain-content">
                                <TweenOne
                                    animation={{ y: 50, opacity: 0, type: 'from', delay: 200 }}
                                    key={'tweenone'+each.id}
                                    name="TweenOne"
                                >
                                    {each.name}
                                </TweenOne>
                                <p key="p" className="contain-content-contain">
                                    {each.content}                                    
                                    
                                </p>
                            </div>
                        </div>
                    </QueueAnim>
                </Element>
                ))}
                    <Thumb prefixCls="user-thumb" key="thumb" component={TweenOne}
                        animation={{ bottom: this.state.enter ? 0 : -70 }}
                    >
                        {thumbChildren}
                    </Thumb>
                </BannerAnim>
            );
        }else{
            return (
                <BannerAnim onMouseEnter={this.onMouseEnter.bind(this)}
                    onMouseLeave={this.onMouseLeave.bind(this)}
                    arrow={this.state.arrowShow}
                    autoPlay={this.state.autoPlay}
                    dragPlay={this.state.dragPlay}
                    autoPlaySpeed={this.state.autoPlaySpeed}>
                </BannerAnim>
            );
        }
    }
}