import React, { Component } from 'react'
import BannerAnim from 'rc-banner-anim';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import { NavLink } from 'react-router-dom';
import { Popover, PopoverHeader, PopoverBody } from 'reactstrap';
import './index.css';
import './thumb.css';
import '../index.css';

import mobile from '../../../assest/image/mobileApplication.jpg';
import web from '../../../assest/image/webApplication.jpg';
import EMS from '../../../assest/image/EducationManagementSystem3.jpg';
import CRM from '../../../assest/image/CRM.jpg';
import '../../../css/header/home.css';

const { Element, Thumb } = BannerAnim;
const BgElement = Element.BgElement;
export default class HomeServices extends Component {
    constructor(props) {
        super(props);
        this.imgArray = [
            EMS,
            CRM,
            mobile,
            web
        ];

        this.state = {
            enter: true,
            isLoaded: false,
            arrowShow: false,
            autoPlay: false,
            autoPlaySpeed: 5000,
            dragPlay: false,
            data:[],
            // serviceContent: 'Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.',

        };
    }
    componentDidMount(){
        this.setState({
            popoverOpen: true
        });
        axios.get('/api/service').then(response => {
            this.setState({
              data:response.data,
              isLoaded:true
             });
         }).catch(errors => {
             console.log(errors);
         });
    }

    onMouseEnter() {
        this.setState({
            enter: true,
        });
    }

    onMouseLeave() {
        this.setState({
            enter: true,
        });
    }

    render() {
        const thumbChildren = this.imgArray.map((img, i) =>
            <span className="thumbnail-services" key={i}><i style={{ backgroundImage: `url(${img})` }} /></span>
        );
        var {isLoaded,data}=this.state;
        if(isLoaded){
        return (
            <BannerAnim onMouseEnter={this.onMouseEnter.bind(this)}
                onMouseLeave={this.onMouseLeave.bind(this)}
                arrow={this.state.arrowShow}
                autoPlay={this.state.autoPlay}
                dragPlay={this.state.dragPlay}
                autoPlaySpeed={this.state.autoPlaySpeed}>
                {data.map(each=>(
                    <Element key={'element'+each.id}
                    prefixCls="banner-user-elem"
                >
                    <BgElement
                        key={"bgelement"+each.id}
                        className="bg"
                        style={{
                            backgroundImage: `linear-gradient(
                                rgba(200, 15, 200, 0.7),
                                rgba(200, 15, 200, 0.7)
                              ),url(${this.imgArray[0]})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                        }}
                    />
                    <QueueAnim key={'queneanim'+each.id} name="QueueAnim" >
                        <div style={{
                            marginTop: '70px',
                            width: 'inherit',
                            display: 'flex',
                            color:'#FFFFFF',
                            background: 'transparent',
                            flexDirection: 'column',
                            justifyContent: 'justify',
                            alignItems: 'center'
                        }}>
                            <h1 key="h1" className="title-content">Services We Provide</h1>
                            <div className="contain-content">
                                <TweenOne
                                    animation={{ y: 50, opacity: 0, type: 'from', delay: 200 }}
                                    key={"tweenone"+each.id}
                                    name="TweenOne"
                                >
                                    {each.sub_services}
                                </TweenOne>
                                <p key="p" className="contain-content-contain">
                                    {each.content}                                    
                                    
                                </p>
                                <NavLink to={each.link}>
                                 <button style={{
                                    background:'transparent',
                                    borderStyle:'solid',
                                    borderColor:'white',
                                    color:'white',
                                    borderWidth:'1px',
                                    padding:'3px',
                                    paddingRight:'10px',
                                    paddingLeft:'10px',
                                    cursor:'pointer'
                                }}>
                                    Read More
                                </button>
                                </NavLink>
                            </div>
                        </div>
                    </QueueAnim>
                </Element>
                ))}
                {/* 
                <Element key="bbb"
                    prefixCls="banner-user-elem"
                >
                    <BgElement
                        key="bg"
                        className="bg"
                        style={{
                            backgroundImage: `linear-gradient(
                                rgba(200, 15, 200, 0.7),
                                rgba(200, 15, 200, 0.7)
                              ),url(${this.imgArray[1]})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                        }}
                    />
                    <QueueAnim key="1" name="QueueAnim" >
                        <div style={{
                            marginTop: '70px',
                            width: 'inherit',
                            display: 'flex',
                            color:'#FFFFFF',
                            background: 'transparent',
                            flexDirection: 'column',
                            justifyContent: 'justify',
                            alignItems: 'center'
                        }}>
                            <h1 key="h1" className="title-content">Services We Provide</h1>
                            <div className="contain-content">
                                <TweenOne
                                    animation={{ y: 50, opacity: 0, type: 'from', delay: 200 }}
                                    key="2"
                                    name="TweenOne"
                                >
                                    Customer Relationship Management (CRM)
                                </TweenOne>
                                <p key="p" className="contain-content-contain">
                                    {this.state.serviceContent}
                                    {this.state.serviceContent}
                                </p>
                                <button style={{
                                    background:'transparent',
                                    borderStyle:'solid',
                                    borderColor:'white',
                                    color:'white',
                                    borderWidth:'1px',
                                    padding:'3px',
                                    paddingRight:'10px',
                                    paddingLeft:'10px',
                                    cursor:'pointer'
                                }}>
                                    View More
                                </button>
                            </div>
                        </div>
                    </QueueAnim>

                </Element>
                <Element key="ccc"
                    prefixCls="banner-user-elem"
                >
                    <BgElement
                        key="bg"
                        className="bg"
                        style={{
                            backgroundImage: `linear-gradient(
                                rgba(200, 15, 200, 0.7),
                                rgba(200, 15, 200, 0.7)
                              ),url(${this.imgArray[2]})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                        }}
                    />
                    <QueueAnim key="1" name="QueueAnim" >
                        <div style={{
                            marginTop: '70px',
                            width: 'inherit',
                            display: 'flex',
                            color:'#FFFFFF',
                            background: 'transparent',
                            flexDirection: 'column',
                            justifyContent: 'justify',
                            alignItems: 'center'
                        }}>
                            <h1 key="h1" className="title-content">Services We Provide</h1>
                            <div className="contain-content">
                                <TweenOne
                                    animation={{ y: 50, opacity: 0, type: 'from', delay: 200 }}
                                    key="2"
                                    name="TweenOne"
                                >
                                    Mobile Application Development
                                </TweenOne>
                                <p key="p" className="contain-content-contain">
                                    {this.state.serviceContent}
                                    {this.state.serviceContent}
                                </p>
                                <button style={{
                                    background:'transparent',
                                    borderStyle:'solid',
                                    borderColor:'white',
                                    color:'white',
                                    borderWidth:'1px',
                                    padding:'3px',
                                    paddingRight:'10px',
                                    paddingLeft:'10px',
                                    cursor:'pointer'
                                }}>
                                    View More
                                </button>
                            </div>
                        </div>
                    </QueueAnim>

                </Element>
                {/* <Element key="bbb"
                    prefixCls="banner-user-elem"
                >
                    <BgElement
                        key="bg"
                        className="bg"
                        style={{
                            backgroundImage: `linear-gradient(
                                rgba(255, 15, 215, 0.5),
                                rgba(255, 15, 215, 0.5)
                              ),url(${this.imgArray[1]})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center',
                        }}
                    />
                    <QueueAnim key="1" name="QueueAnim">
                        <div style={{
                            marginTop: '70px',
                            width: 'inherit',
                            display: 'flex',
                            color:'#FFFFFF',
                            flexDirection: 'column',
                            justifyContent: 'justify',
                            alignItems: 'center'
                        }}>
                            <h1 key="h1">Services We Provide</h1>
                            <div style={{
                                width: 'fit-content',
                                alignSelf: 'flex-start',
                                padding: '60px',
                                height: '400px',
                                background: 'transparent'
                            }}>
                                <TweenOne
                                    animation={{ y: 50, opacity: 0, type: 'from', delay: 200 }}
                                    key="2"
                                    name="TweenOne"
                                >
                                    Web Development
                                </TweenOne>
                                <p key="p" style={{ paddingTop: '20px' }}>
                                    {this.state.serviceContent}
                                    {this.state.serviceContent}
                                </p>
                            </div>
                        </div>
                    </QueueAnim>

                </Element> 
                <Element key="ddd"
                    prefixCls="banner-user-elem"
                >
                    <BgElement
                        key="bg"
                        className="bg"
                        style={{
                            backgroundImage: `linear-gradient(
                                rgba(200, 15, 200, 0.7),
                                rgba(200, 15, 200, 0.7)
                              ),url(${this.imgArray[3]})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                        }}
                    />
                    <QueueAnim key="1" name="QueueAnim">
                        <div style={{
                            marginTop: '70px',
                            width: 'inherit',
                            display: 'flex',
                            color:'#FFFFFF',
                            background: 'transparent',
                            flexDirection: 'column',
                            justifyContent: 'justify',
                            alignItems: 'center'
                        }}>
                            <h1 key="h1" className="title-content">Services We Provide</h1>
                            <div className="contain-content">
                                <TweenOne
                                    animation={{ y: 50, opacity: 0, type: 'from', delay: 200 }}
                                    key="2"
                                    name="TweenOne"
                                >
                                    Web Development
                                </TweenOne>
                                <p key="p" className="contain-content-contain">
                                    {this.state.serviceContent}
                                    {this.state.serviceContent}
                                </p>
                                <button style={{
                                    background:'transparent',
                                    borderStyle:'solid',
                                    borderColor:'white',
                                    color:'white',
                                    borderWidth:'1px',
                                    padding:'3px',
                                    paddingRight:'10px',
                                    paddingLeft:'10px',
                                    cursor:'pointer'
                                }}>
                                    View More
                                </button>
                            </div>
                        </div>
                    </QueueAnim>

                </Element> */}
                <Thumb id="btnPop"  prefixCls="user-thumb" key="thumb" component={TweenOne}
                    animation={{ bottom: this.state.enter ? 0 : -70 }}
                >
                    {thumbChildren}
                </Thumb>
                
            </BannerAnim>
        );
            }else{
                return (
                    <BannerAnim onMouseEnter={this.onMouseEnter.bind(this)}
                        onMouseLeave={this.onMouseLeave.bind(this)}
                        arrow={this.state.arrowShow}
                        autoPlay={this.state.autoPlay}
                        dragPlay={this.state.dragPlay}
                        autoPlaySpeed={this.state.autoPlaySpeed}>
                        {/* <Element key="aaa"
                            prefixCls="banner-user-elem"
                        >
                            <BgElement
                                key="bg"
                                className="bg"
                                style={{
                                    backgroundImage: `linear-gradient(
                                        rgba(200, 15, 200, 0.7),
                                        rgba(200, 15, 200, 0.7)
                                      ),url(${this.imgArray[0]})`,
                                    backgroundSize: 'cover',
                                    backgroundPosition: 'center center',
                                }}
                            />
                            <QueueAnim key="1" name="QueueAnim" >
                                <div style={{
                                    marginTop: '70px',
                                    width: 'inherit',
                                    display: 'flex',
                                    color:'#FFFFFF',
                                    background: 'transparent',
                                    flexDirection: 'column',
                                    justifyContent: 'justify',
                                    alignItems: 'center'
                                }}>
                                    <h1 key="h1" className="title-content">Services We Provide</h1>
                                    <div className="contain-content">
                                        <TweenOne
                                            animation={{ y: 50, opacity: 0, type: 'from', delay: 200 }}
                                            key="2"
                                            name="TweenOne"
                                        >
                                            Education Management System (EMS)
                                        </TweenOne>
                                        <p key="p" className="contain-content-contain">
                                            {this.state.serviceContent}
                                            {this.state.serviceContent}
                                            
                                            
                                        </p>
                                        <button style={{
                                            background:'transparent',
                                            borderStyle:'solid',
                                            borderColor:'white',
                                            color:'white',
                                            borderWidth:'1px',
                                            padding:'3px',
                                            paddingRight:'10px',
                                            paddingLeft:'10px',
                                            cursor:'pointer'
                                        }}>
                                            View More
                                        </button>
                                    </div>
                                </div>
                            </QueueAnim>
                        </Element>
                        <Element key="bbb"
                            prefixCls="banner-user-elem"
                        >
                            <BgElement
                                key="bg"
                                className="bg"
                                style={{
                                    backgroundImage: `linear-gradient(
                                        rgba(200, 15, 200, 0.7),
                                        rgba(200, 15, 200, 0.7)
                                      ),url(${this.imgArray[1]})`,
                                    backgroundSize: 'cover',
                                    backgroundPosition: 'center center',
                                }}
                            />
                            <QueueAnim key="1" name="QueueAnim" >
                                <div style={{
                                    marginTop: '70px',
                                    width: 'inherit',
                                    display: 'flex',
                                    color:'#FFFFFF',
                                    background: 'transparent',
                                    flexDirection: 'column',
                                    justifyContent: 'justify',
                                    alignItems: 'center'
                                }}>
                                    <h1 key="h1" className="title-content">Services We Provide</h1>
                                    <div className="contain-content">
                                        <TweenOne
                                            animation={{ y: 50, opacity: 0, type: 'from', delay: 200 }}
                                            key="2"
                                            name="TweenOne"
                                        >
                                            Customer Relationship Management (CRM)
                                        </TweenOne>
                                        <p key="p" className="contain-content-contain">
                                            {this.state.serviceContent}
                                            {this.state.serviceContent}
                                        </p>
                                        <button style={{
                                            background:'transparent',
                                            borderStyle:'solid',
                                            borderColor:'white',
                                            color:'white',
                                            borderWidth:'1px',
                                            padding:'3px',
                                            paddingRight:'10px',
                                            paddingLeft:'10px',
                                            cursor:'pointer'
                                        }}>
                                            View More
                                        </button>
                                    </div>
                                </div>
                            </QueueAnim>
        
                        </Element>
                        <Element key="ccc"
                            prefixCls="banner-user-elem"
                        >
                            <BgElement
                                key="bg"
                                className="bg"
                                style={{
                                    backgroundImage: `linear-gradient(
                                        rgba(200, 15, 200, 0.7),
                                        rgba(200, 15, 200, 0.7)
                                      ),url(${this.imgArray[2]})`,
                                    backgroundSize: 'cover',
                                    backgroundPosition: 'center center',
                                }}
                            />
                            <QueueAnim key="1" name="QueueAnim" >
                                <div style={{
                                    marginTop: '70px',
                                    width: 'inherit',
                                    display: 'flex',
                                    color:'#FFFFFF',
                                    background: 'transparent',
                                    flexDirection: 'column',
                                    justifyContent: 'justify',
                                    alignItems: 'center'
                                }}>
                                    <h1 key="h1" className="title-content">Services We Provide</h1>
                                    <div className="contain-content">
                                        <TweenOne
                                            animation={{ y: 50, opacity: 0, type: 'from', delay: 200 }}
                                            key="2"
                                            name="TweenOne"
                                        >
                                            Mobile Application Development
                                        </TweenOne>
                                        <p key="p" className="contain-content-contain">
                                            {this.state.serviceContent}
                                            {this.state.serviceContent}
                                        </p>
                                        <button style={{
                                            background:'transparent',
                                            borderStyle:'solid',
                                            borderColor:'white',
                                            color:'white',
                                            borderWidth:'1px',
                                            padding:'3px',
                                            paddingRight:'10px',
                                            paddingLeft:'10px',
                                            cursor:'pointer'
                                        }}>
                                            View More
                                        </button>
                                    </div>
                                </div>
                            </QueueAnim>
        
                        </Element>
                        {/* <Element key="bbb"
                            prefixCls="banner-user-elem"
                        >
                            <BgElement
                                key="bg"
                                className="bg"
                                style={{
                                    backgroundImage: `linear-gradient(
                                        rgba(255, 15, 215, 0.5),
                                        rgba(255, 15, 215, 0.5)
                                      ),url(${this.imgArray[1]})`,
                                    backgroundSize: 'cover',
                                    backgroundPosition: 'center',
                                }}
                            />
                            <QueueAnim key="1" name="QueueAnim">
                                <div style={{
                                    marginTop: '70px',
                                    width: 'inherit',
                                    display: 'flex',
                                    color:'#FFFFFF',
                                    flexDirection: 'column',
                                    justifyContent: 'justify',
                                    alignItems: 'center'
                                }}>
                                    <h1 key="h1">Services We Provide</h1>
                                    <div style={{
                                        width: 'fit-content',
                                        alignSelf: 'flex-start',
                                        padding: '60px',
                                        height: '400px',
                                        background: 'transparent'
                                    }}>
                                        <TweenOne
                                            animation={{ y: 50, opacity: 0, type: 'from', delay: 200 }}
                                            key="2"
                                            name="TweenOne"
                                        >
                                            Web Development
                                        </TweenOne>
                                        <p key="p" style={{ paddingTop: '20px' }}>
                                            {this.state.serviceContent}
                                            {this.state.serviceContent}
                                        </p>
                                    </div>
                                </div>
                            </QueueAnim>
        
                        </Element> 
                        <Element key="ddd"
                            prefixCls="banner-user-elem"
                        >
                            <BgElement
                                key="bg"
                                className="bg"
                                style={{
                                    backgroundImage: `linear-gradient(
                                        rgba(200, 15, 200, 0.7),
                                        rgba(200, 15, 200, 0.7)
                                      ),url(${this.imgArray[3]})`,
                                    backgroundSize: 'cover',
                                    backgroundPosition: 'center center',
                                }}
                            />
                            <QueueAnim key="1" name="QueueAnim">
                                <div style={{
                                    marginTop: '70px',
                                    width: 'inherit',
                                    display: 'flex',
                                    color:'#FFFFFF',
                                    background: 'transparent',
                                    flexDirection: 'column',
                                    justifyContent: 'justify',
                                    alignItems: 'center'
                                }}>
                                    <h1 key="h1" className="title-content">Services We Provide</h1>
                                    <div className="contain-content">
                                        <TweenOne
                                            animation={{ y: 50, opacity: 0, type: 'from', delay: 200 }}
                                            key="2"
                                            name="TweenOne"
                                        >
                                            Web Development
                                        </TweenOne>
                                        <p key="p" className="contain-content-contain">
                                            {this.state.serviceContent}
                                            {this.state.serviceContent}
                                        </p>
                                        <button style={{
                                            background:'transparent',
                                            borderStyle:'solid',
                                            borderColor:'white',
                                            color:'white',
                                            borderWidth:'1px',
                                            padding:'3px',
                                            paddingRight:'10px',
                                            paddingLeft:'10px',
                                            cursor:'pointer'
                                        }}>
                                            View More
                                        </button>
                                    </div>
                                </div>
                            </QueueAnim>
        
                        </Element> */}
                        <Thumb id="btnPop"  prefixCls="user-thumb" key="thumb" component={TweenOne}
                            animation={{ bottom: this.state.enter ? 0 : -70 }}
                        >
                            {thumbChildren}
                        </Thumb>
                        
                    </BannerAnim>
                );
            }
    }
}