import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Navbar_Component from '../components/Navbar/Navbar.js';
import FullWidthModal from '../components/Navbar/Navbar.js';
import { HashRouter as Router, Route, Link } from 'react-router-dom';

//css import here..
import '../../css/main.css';
import '../../css/modal/full-width-modal.css';

export default class Main extends Component {
    constructor(props) {
        super(props);
        this.togglePopover = this.togglePopover.bind(this);
        this.handleToggle = this.handleToggle.bind(this);
        this.renderConditionalView = this.renderConditionalView.bind(this);
        this.state = {
            popoverOpen: false,
            check: false,
        };
    }

    handleToggle(e) {
        this.setState({
            check: e
        })
    }

    renderConditionalView() {
        // const c = this.state.check;
        // if (c) {
        //     return (
                
        //     );
        // } else {
        //     return (
        //         <div>i am false</div>
        //     );
        // }
    }
    togglePopover() {
        this.setState({
            popoverOpen: !this.state.popoverOpen,
        })
    }
    render() {
        return (
            <div id="mainView">
                <Navbar_Component handleToggle={this.handleToggle}></Navbar_Component> 
                              
                {/* <this.renderConditionalView style={{ position: 'absolute' }} /> */}
                
                {/* <div id='main' className=''>
                    <Button id="btnPop" onClick={this.togglePopover} color="primary">Click Me!</Button>
                    <Popover placement="bottom" isOpen={this.state.popoverOpen} target="btnPop" toggle={this.togglePopover}>
                        <PopoverHeader>Popover Title</PopoverHeader>
                        <PopoverBody>Sed posuere consectetur est at lobortis. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</PopoverBody>
                    </Popover>
                    
                </div> */}
            </div>
        );
    }
}


if (document.getElementById('main')) {
    ReactDOM.render(<Main />, document.getElementById('main'));
}
