import React, { Component } from 'react';
import { Button } from 'reactstrap';
import '../../../css/sidebar/sidebar.css';
import Navbar_Component from './Navbar';

export default class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.controlNavbar = this.controlNavbar.bind(this);
        this.renderNavbar = this.renderNavbar.bind(this);
        this.state = {
            check: false,
            autoString: '<-',
        }
    }

    controlNavbar(e) {
        this.setState({
            check: !this.state.check,
        })
        // console.log(this.state.check);      
    }
    renderNavbar() {
        const check_temp = this.state.check;
        if (this.state.check) {
            
            return (
                <Navbar_Component></Navbar_Component>
            );

        } else { 
            return (
                <div></div>
            );
        }
    }
    render() {

        return (
            <div id="navbar-main">
                <this.renderNavbar/>
                <div id='sidebar'>
                    <Button id='btnMenu' onClick={() => this.controlNavbar(true)}>
                        {this.state.autoString}
                    </Button>
                </div>
            </div>

        );
    }
}