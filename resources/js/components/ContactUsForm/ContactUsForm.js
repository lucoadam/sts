import React, { Component } from 'react';
import '../../../css/contact/contact.css';
export default class ContactUs extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <form onSubmit={this.props.submitInquiry} className="needs-validation">
                <div className="form-group">
                    <label htmlFor="name" className="label-style">Full Name:</label>
                    <input placeholder="Full Name *"
                        type="name"
                        name="names"
                        className="input-style"
                        id="name"
                        required
                        value={this.props.names}
                        onChange={this.props.handleNameChange}
                    ></input>
                </div>
                <div className="form-group" >
                    <label htmlFor="email" className="label-style">Email:</label>
                    <input placeholder="Email Address *"
                        type="email"
                        name="emails"
                        className="input-style "
                        id="email"
                        required
                        value={this.props.emails}
                        onChange={this.props.handleEmailChange}
                    />
                    <small id="emailHelp" className="form-text" >We'll never share your email with anyone else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="phone" className="label-style">Phone:</label>
                    <input placeholder="Phone Number *"
                        type="number"
                        name="phoneNumbers"
                        className="input-style "
                        id="phone"
                        required
                        value={this.props.phoneNumbers}
                        onChange={this.props.handleNumberChange}
                    />
                   
                    <small id="emailHelp" className="form-text" >We'll never share your phone number with anyone else.</small>
                </div>
                <div className="form-group">
                    <label htmlFor="description" className="label-style"
                        style={{ paddingTop: '10px', paddingBottom: '10px' }}>Breif Description:</label>
                    <textarea className="form-control contact-text-area"
                        rows="5" id="discription"
                        name="descriptions"
                        style={{ borderColor: 'black' }}
                        value={this.props.descriptions}
                        placeholder="Brief Descriptionof your project/requirements"
                        onChange={this.props.handleDescriptionChange}
                    ></textarea>
                </div>
                <div className="form-group">
                    {/* <label htmlFor="projectType" className="label-style">Project Type:</label>
                                        <input placeholder="Type of Project" type="text" className="input-style " id="projectType" /> */}
                    <label htmlFor="projectType" className="label-style">Select Project Type:</label>
                    <select className="form-control dropdown-project-type"
                        id="projectType"
                        name="projectTypes"
                        placeholder="Type of Project"
                        type="text"
                        value={this.props.projectTypes}
                        onChange={this.props.handleProjectTypeChange}
                    >
                        <option>EMS</option>
                        <option>CRM</option>
                        <option>Web Application</option>
                        <option>Mobile Application</option>
                    </select>
                </div>
                <button type="submit" className="btn-submit-contact">Send Inquiry</button>
            </form>
        );
    }
}