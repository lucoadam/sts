import React,{Component} from 'react';
import '../../../css/services/eachservicestyle.css';
import { ChevronRight } from 'react-bytesize-icons';
import { NavLink } from 'react-router-dom';

export default class Service extends Component{
    constructor(props){
      super(props);
      
      this.state={
        data:[],
        isLoaded:false
      }
    }
    componentDidMount(){
        axios.get('api/service/'+this.props.match.params.name)
        .then(res=>{
            this.setState({
                data:res.data,
                isLoaded: true
            });
        })
    }
    render(){
      var {data,isLoaded} = this.state;
     
      if(isLoaded){
        return (
          <div className="serviceWrapper">
            <div className="serviceNavBar">
                <div><NavLink to="/services">
                  <span className='header'>Services</span>
                </NavLink>  /   <span> {data.sub_services} </span> </div>
            </div>
            <div className="serviceHeader" style={{backgroundImage: `url(${require('../../../assest/image/services/headerBackground.jpg')})`}}>
              <span style={{color:'#fff'}}>{data.sub_services}</span>
            </div>
            <div className="serviceContent">
              <span>{data.content}</span>
            </div>

            <div className="serviceSubContentMain">

              <div className="serviceSubContent">
                <div className="serviceSubContentHeader">
                  Proactive Approach Toward Quality Assurance
                </div>
                <div className="serviceSubContentContent">
                  {data.content}
                </div>
              </div>
              <div className="serviceSubContent">
                <div className="serviceSubContentHeader">
                 Defect-Free Delivery Through Independent Testing
                </div>
                <div className="serviceSubContentContent">
                  Whether it is functional or non-functional testing, manual or automated approaches, we have experience in testing multiple platforms and complex software systems such as enterprise content management systems or high-traffic eCommerce websites. We also have well-defined QA processes for the different methodologies of software development, be it RUP, agile, or waterfall.
                </div>
              </div>
              <div className="serviceSubContent">
                <div className="serviceSubContentHeader">
                  Testing Methodology
                </div>
                <div className="serviceSubContentContent">
                  Our testing methodology incorporates the continual process of measuring, monitoring, and feedback analysis to ensure that we prevent errors in an ongoing development process. Agile testing methodology, close collaboration with clients, and efficient change management process enable us to quickly roll out quality results.
                </div>
              </div>
              <div className="serviceSubContent">
                <div className="serviceSubContentHeader">
                  Quality Assurance Tools
                </div>
                <div className="serviceSubContentContent">
                  TWe have a dedicated device lab and proprietary tools that enable us to reduce your overhead costs and improve time-to-market. We may also develop plug and play QA solutions based on our client's need. Our team of certified (CSTE, ISTQB, CSQA, CMST) testing professionals will help identify the best solution for your application.
                </div>
              </div>

            </div>
            <div className="testingFirstWrapBorder">
            <div>
                <h3>Functional Testing</h3>
                <div className="testlingtwoList">
                    <ul className="listText listTextCommon">
                        <li>
                            <span className="listCont">Smoke Testing</span>
                        </li>
                        <li>
                            <span className="listCont">Regression Testing</span>
                        </li>
                        <li>
                            <span className="listCont">Mobile CSS Testing</span>
                        </li>
                        <li>
                            <span className="listCont">Automation Testing</span>
                        </li>
                        <li>
                            <span className="listCont">Quality of Data testing</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div>
                <h3 className="second">Non-Functional Testing</h3>
                <div className="testlingtwoList">
                    <ul className="listText listTextCommon">
                        <li>
                            <span className="listCont">Security Testing</span>
                        </li>
                        <li>
                            <span className="listCont">SEO Testing</span>
                        </li>
                        <li>
                            <span className="listCont">Compatibility Testing</span>
                        </li>
                        <li>
                            <span className="listCont">Performance Testing</span>
                        </li>
                        <li>
                            <span className="listCont">Usability Testing</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

          </div>
        );
      }else{
          return <div>Loading...</div>;
      }
    }
}
