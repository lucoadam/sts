import React,{Component} from 'react';
import { NavLink } from 'react-router-dom';
import '../../../css/services/style.css';
import { ChevronRight } from 'react-bytesize-icons';
export default class Services extends Component{
    constructor(){
      super();
      this.state={
        data:[],
        isLoaded:false
      };
    }
    componentDidMount(){
      axios.get('/api/service').then(response => {
         this.setState({
           data:response.data,
           isLoaded:true
          });
      }).catch(errors => {
          console.log(errors);
      });
    }
    render(){
        var {data,isLoaded} =this.state;
        if(isLoaded){
          return(
            <div className='sectionContent'>
                <div className='sectionContentHeader'>
                  <span>What we do</span>
                </div>
                <div className='sectionContentMain'>
                    {data.map(response=>(
                   <div key={response.id} className='content'>
                      <div className='contentImage'>
                        <img src={require('../../../assest/image/services/devops.png')} alt='img1'/>
                      </div>
                      <div className='mainContent'>
                          <span className='mainContentHeader'>
                            {response.sub_services}
                          </span>
                          <span className='mainContentInfo'>
                            {response.content}
                          </span>
                      </div>
                      <div className='mainContentButton'>
                        <span><NavLink to={'services/'+response.id}>Read More<ChevronRight width={15} height={15}/></NavLink></span>
                      </div>
                    </div>
                    ))}
                </div>
            </div>
          );
        }else{
          return(
            <div className='sectionContent'>
                <div className='sectionContentHeader'>
                  <span>What we do</span>
                </div>
                <div className='sectionContentMain'>
                </div>
            </div>
          );
        }
   }
}