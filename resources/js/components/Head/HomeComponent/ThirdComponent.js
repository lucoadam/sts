import React, { Component } from 'react';
import '../../../../css/header/header.css';
import { FacebookLoginButton, GoogleLoginButton, TwitterLoginButton, InstagramLoginButton } from "react-social-login-buttons";
import { SocialIcon } from 'react-social-icons';
import Icon from 'react-geomicons';
import { Twitter, Location, Telephone, Message, Mail } from 'react-bytesize-icons';
import MaterialIcon, {colorPalette} from 'material-icons-react';
export default class ThirdComponent extends Component {
    render() {
        return (
            <div className="fullPageSectionSecond">
                <div className="card-group-container" >
                    <div style={{ display: 'flex', flexDirection: 'row', textAlign: 'center', alignItems: 'center', justifyContent: 'center' }}>
                        <h2>Our Features</h2>
                    </div>
                    <div class="card-group" >
                        <div class="card">
                            <div class="card-body text-center"  >
                                <img src={require('../../../../assest/image/Mobile-Application-Development.png')}
                                    style={{ width: '100%', height: 'auto' }}></img>
                            </div>
                        </div>
                        <div class="card ">
                            <div class="card-body">
                                <h2>The Complete Digital Solution</h2>
                                <h5>QBurst is known for its full-stack development package. With focus teams for every layer of the application—from fluidly responsive screens to powerful data crunching engines and analytical subsystems—we deliver a rock-solid solution.
Our front-end engineers work with the latest JS frameworks and libraries to bring mockups from development to delivery. To build the application brain, skilled backend developers team up with database experts and experienced data scientists. On the cloud or otherwise, we build you resilient systems meshing the various components together with suitable middleware.
For businesses that prefer everything under one roof, we offer design, usability, and other complementary services along with custom application development.</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card-group" style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap-reverse' }}>
                        <div class="card ">
                            <div class="card-body ">
                                <h2>Inventive Use of Technology</h2>
                                <h5>Ethereum smart contracts, AI travel assistants, or virtual reality shopping applications—we enable you to stay ahead of the times through the seamless adoption of technology. Together, let us identify and explore existing and emerging technology to drive innovation.
In an age characterised by digital data deluge and machine intelligence, we help you tap your data. From HTML5 to React and AngularJS, the evolving frontend ecosystem also offers multiple choices. We can help strike a balance between technology and user expectations to build a productive application.</h5>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body text-center"  >
                                <img src={require('../../../../assest/image/webApplicationDevelopment.png')}
                                    style={{ width: '100%', height: 'auto' }}></img>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}