import React, { Component } from 'react';
import MessageForm from './MessageForm/MessageForm.js';
import scrollToComponent from 'react-scroll-to-component';
import { FacebookLoginButton, GoogleLoginButton, TwitterLoginButton, InstagramLoginButton } from "react-social-login-buttons";
import { SocialIcon } from 'react-social-icons';
import Icon from 'react-geomicons';
import { Twitter, Location, Telephone, Message, Mail } from 'react-bytesize-icons';
import MaterialIcon, { colorPalette } from 'material-icons-react';
import MapContainer from './MapComponent/MapContainer.js';
import "fullpage.js/vendors/scrolloverflow"; // Optional. When using scrollOverflow:true
import ReactFullpage from "@fullpage/react-fullpage";
import "react-bootstrap-carousel/dist/react-bootstrap-carousel.css";
import RBCarousel from "react-bootstrap-carousel";
import HomeFeature from '../HomeContainer/HomeFeature';
import HomeServices from '../HomeContainer/HomeServices';
import MainFooter from '../FooterComponent/MainFooter';


//animation helper
import posed, { PoseGroup } from "react-pose";
import styled from "styled-components";
import { tween } from "popmotion";

import '../../../css/header/home.css';

// NOTE: if using fullpage extensions/plugins put them here and pass it as props
const pluginWrapper = () => {
    /**
     * require('fullpage.js/vendors/scrolloverflow'); // Optional. When using scrollOverflow:true
     */
};

const styles = { height: '500px', width: "100%" };

const originalColors = ['#282c34', '#ff5f45', '#0798ec'];
const messageFormStyle = posed.div({
    fullscreen: {
        width: 'fit-content',
        height: 'fit-content',
        bottom: '5%',
        right: '4vw',
        transition: { tween, duration: 1000, type: 'spring', stiffness: 100 }
    },
    idle: {
        width: 'fit-content',
        height: 'fit-content',
        bottom: '-500%',
        right: '4vw',
        transition: tween
    }
});

const StyledMessageForm = styled(messageFormStyle)`
    position: fixed;
    background: rgba(0, 0, 0, 0.4);
    bottom: 5%;
    right: 4vw;
    border-radius: 0 0 40px 0;
    z-index: 99999;
`;

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.handleOnClickPrev = this.handleOnClickPrev.bind(this);
        this.handleOnClickNext = this.handleOnClickNext.bind(this);
        this.updateSlideNumber = this.updateSlideNumber.bind(this);
        this.displayBottomToTopButton = this.displayBottomToTopButton.bind(this);
        this.handleMessageForm = this.handleMessageForm.bind(this);
        this.viewMessageForm = this.viewMessageForm.bind(this);
        this.displayOrHideSocialSite = this.displayOrHideSocialSite.bind(this);
        this.handleGetInTouch = this.handleGetInTouch.bind(this);
        this.state = {
            isOpen: false,
            prevNext: 1,
            isMiddle: true,
            isTop: true,
            isBottom: true,
            isWhere: true,
            hideOrShowBottomToTop: false,
            displayMessageForm: false,
            active: false,
            change: false,
            hrefGoogle: 'http://www.google.com',
            hrefFacebook: 'http://www.facebook.com',
            hreTwitter: 'http://www.twitter.com',
            hrefInstagram: 'http://www.instagram.com',
            leftBrace: '{',
            rightBrace: '}',
            displaySocialSite: '0',
            sectionsColor: [...originalColors],
            fullpages: [
                {
                    text: 'section 1',
                },
                {
                    text: 'section 2',
                },
            ],
            autoplay: true,
            true: true,
            false: false,
            easing: 'easeInOutBack',
            easingcss3: 'easeInOutBack',
            bigSectionsDestination: 'top',
        };
        this.fullpageApi = null;
    }

    onLeave(origin, destination, direction) {
        // console.log('onLeave', { origin, destination, direction});
        this.setState({
            prevNext: destination.index + 1
        })
        if (destination.index == 3) {
            this.setState({
                displaySocialSite: '-100%'
            })
        } else {
            this.setState({
                displaySocialSite: '0'
            })
        }
    }

    componentDidMount() {
        document.addEventListener('scroll', () => {
            const isTop = window.scrollY < 100;
            if (isTop !== this.state.isTop) {
                this.setState({ isTop })
                // console.log(this.state.isTop);
                this.updateSlideNumber();
            }
        });
        document.addEventListener('scroll', () => {
            const isMiddle = window.scrollY < 800;
            if (isMiddle !== this.state.isMiddle) {
                this.setState({ isMiddle })
                // console.log(this.state.isMiddle);
                this.updateSlideNumber();
            }

        });
        document.addEventListener('scroll', () => {
            const isBottom = window.scrollY < 1500;
            if (isBottom !== this.state.isBottom) {
                this.setState({ isBottom })
                // console.log(this.state.isBottom);
                this.updateSlideNumber();
                this.displayOrHideSocialSite();
            }
        });

    }

    displayOrHideSocialSite() {
        if (this.state.isBottom) {
            this.setState({
                displaySocialSite: 'block'
            })
        } else {
            this.setState({
                displaySocialSite: 'none'
            })
        }
        console.log(this.state.displaySocialSite);
    }

    updateSlideNumber() {
        setTimeout(() => {
            //top of page i.e. page 1
            if (this.state.isTop && this.state.isMiddle && this.state.isBottom) {
                this.setState({
                    prevNext: 0,
                    hideOrShowBottomToTop: false,
                })
                // console.log(this.state.hideOrShowBottomToTop);
            }
            // page 2
            if (!this.state.isTop && this.state.isMiddle && this.state.isBottom) {
                this.setState({
                    prevNext: 1,
                    hideOrShowBottomToTop: false,
                })
                // console.log(this.state.hideOrShowBottomToTop);
            }
            //page3 
            if (!this.state.isTop && !this.state.isMiddle && this.state.isBottom) {
                this.setState({
                    prevNext: 2,
                    hideOrShowBottomToTop: true
                })
                // console.log(this.state.hideOrShowBottomToTop);
            }
            //bottom of page i.e. page 3
            if (!this.state.isTop && !this.state.isMiddle && !this.state.isBottom) {
                this.setState({
                    prevNext: 3,
                    hideOrShowBottomToTop: true
                })
                // console.log(this.state.hideOrShowBottomToTop);
            }
        }, 500)
    }

    displayBottomToTopButton() {
        if (this.state.hideOrShowBottomToTop) {
            return (
                //Note: ##do not delete
                // <div className="_bottomToTop">
                //     <div className="_ToTop" onClick={() => { scrollToComponent(this.Top, { offset: 0, align: 'top', duration: 1500 }) }}>
                //         <img id="arrow-to-top" src={require('../../../assest/arrowIcon/doubleGoToTop.png')}></img>
                //     </div>
                // </div>
                <ScrollButton scrollStepInPx="1000" delayInMs="0" />
            );
        } else {
            return (
                <div className="_bottomToTop" style={{ display: 'none' }}>
                    <div className="_ToTop" onClick={() => { scrollToComponent(this.Top, { offset: 0, align: 'top', duration: 1500 }) }}>
                        <img id="arrow-to-top" src={require('../../../assest/arrowIcon/doubleGoToTop.png')}></img>
                    </div>
                </div>
            )
        }
    }
    //unused function 
    handleOnClickPrev() {
        setTimeout(() => {
            if (this.state.prevNext == 0) {
                //donothing
            } else {
                this.setState({
                    prevNext: --this.state.prevNext
                })
            }
        }, 1000)
        // console.log(this.state.prevNext);
    }
    //unused function
    handleOnClickNext() {
        setTimeout(() => {
            if (this.state.prevNext == 3) {
                //do nothing
            } else {
                this.setState({
                    prevNext: ++this.state.prevNext
                })
            }
        }, 1000)
        // console.log(this.state.prevNext);
    }

    handleMessageForm() {
        this.setState({
            displayMessageForm: !this.state.displayMessageForm,

        })
    }

    changeEmailIcon() {
        this.setState({
            changeEmailIconOrNot: !this.state.changeEmailIconOrNot
        })
    }

    viewMessageForm() {
        if (this.state.displayMessageForm) {
            return (
                <div className="messageForm" >
                    <MessageForm />
                </div>
            )
        } else {
            return (
                <div className="messageForm" style={{ display: 'none' }}>
                    <MessageForm />
                </div>
            )
        }
    }

    handlePrev() {
        fullpageApi.moveSectionDown()
    }

    handleGetInTouch(){
        // this.fullpageApi.moveTo(3);
        alert('hello iam home ');
    }

    render() {
        // setTimeout(()=>{
        //     console.log(this.props.route.gotoDown);
        // },3000)
        return (
            <div className="App" >
                <div className="sideNextPrev">
                    <div className="_nextPrevContainer">
                        {/* <div className="_Prev" onClick={() => { scrollToComponent(this.Top, { offset: 1, align: 'bottom', duration: 500, ease: 'inCirc' }) }}>
                            <img id="arrow-up" src={require('../../../assest/arrowIcon/singleArrow.png')}></img>
                        </div> */}
                        <div className="_Prev" onClick={() => { this.fullpageApi.moveSectionUp() }}>
                            <img id="arrow-up" src={require('../../../assest/arrowIcon/singleArrow.png')}></img>
                        </div>
                        <div className="countPage" >
                            <div>{this.state.prevNext}</div>
                            <div>/</div>
                            <div>4</div>
                        </div>
                        {/* <div className="_Next" onClick={() => { scrollToComponent(this.Bottom, { offset: 0, align: 'bottom', duration: 500, ease: 'inCirc' }) }}>
                            <img id="arrow-down" src={require('../../../assest/arrowIcon/singleArrow.png')}></img>
                        </div> */}
                        <div className="_Next" onClick={() => { this.fullpageApi.moveSectionDown() }}>
                            <img id="arrow-down" src={require('../../../assest/arrowIcon/singleArrow.png')}></img>
                        </div>
                    </div>
                </div>
                <SocialSite displaySocialSite={this.state.displaySocialSite} />
                {/* <this.viewMessageForm /> */}
                <div className="messageForm" >
                    <StyledMessageForm
                        pose={this.state.active ? "fullscreen" : "idle"}>
                        <MessageForm />
                    </StyledMessageForm>
                </div>

                <div className="emailIcon" onClick={() => { this.setState({ active: !this.state.active }); this.changeEmailIcon(); }} >
                    <Icon id="email-icon" name={this.state.changeEmailIconOrNot ? "close" : "chat"} size='2em' fill="white"></Icon>
                    {/* <SocialIcon id="email-icon" network="email"></SocialIcon> */}
                </div>
                <this.displayBottomToTopButton />

                <ReactFullpage
                    // debug /* Debug logging */
                    scrollOverflow={this.state.true}
                    navigation={this.state.false}
                    controlArrows={this.state.controlArrows}
                    onLeave={this.onLeave.bind(this)}
                    // loopTop={this.state.true}
                    loopBottom={this.state.true}
                    // sectionsColor={this.state.sectionsColor}
                    pluginWrapper={pluginWrapper}
                    resetSliders={this.state.true}
                    // normalScrollElements='.sectionFourthContainer'
                    css3={this.state.true}
                    easing={this.state.easing}
                    // easingcss3={this.state.easingcss3}
                    autoScrolling={this.state.true}
                    // parallax={this.state.true}
                    // parallaxOptions={{ type: 'reveal', percentage: 62, property: 'translate'}}
                    // bigSectionsDestination={this.state.bigSectionsDestination}
                    // normalScrollElementTouchThreshold={3}
                    render={({ state, fullpageApi }) => {
                        this.fullpageApi = fullpageApi;
                        return (
                            <ReactFullpage.Wrapper>
                                {/* {fullpages.map(({ text }) => (
                                <div key={text} className="section">
                                    <h1>{text}</h1>
                                </div>
                            ))} */}

                                <div className="section" id="section1" style={{ position: 'relative', display: 'flex', flexDirection: 'column' }}>
                                    {/* <div classNam="fp-bg"></div> */}
                                    <div style={{ position: 'relative', width: 'inherit', height: 'inherit' }}>
                                        <img id="main-image"
                                            src={require('../../../assest/image/mainBack.jpg')}></img>                                         
                                        <div className="fullPageSectionFirst">
                                            <div className="firstSectionText">
                                                Accelerate Your
                                                Digital Journey
                                                with a full-service provider
                                        </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="section" id="section2" style={{ position: 'relative', display: 'flex', flexDirection: 'column' }}>
                                    {/* <div className="fp-bg"></div> */}
                                    <div style={{ width: '100%', height: 'inherit' }}>
                                        <HomeServices />
                                    </div>                                    
                                </div>
                                <div className="section" id="section3">
                                    {/* <div className="fp-bg"></div> */}
                                    <div style={{ width: '100%', height: 'inherit' }}>
                                        <HomeFeature />
                                    </div>                                   
                                </div>
                                <div className="section" id="section4">
                                    <div className="fp-bg"></div>
                                    <MainFooter />
                                </div>
                            </ReactFullpage.Wrapper>
                        )
                    }}
                />
            </div>

        );
    }
}
const Row = props => <div className="row">{props.children}</div>;
const Col = props => (
    <div className={`col-${props.span}`} style={props.style}>
        {props.children}
    </div>
);

class SocialSite extends Component {
    constructor() {
        super();
        this.state = {
            btnSize: '35px',
            hrefGoogle: 'http://www.google.com',
            hrefFacebook: 'http://www.facebook.com',
            hreTwitter: 'http://www.twitter.com',
            hrefInstagram: 'http://www.instagram.com',

        }
    }
    render() {
        return (
            // display: this.props.displaySocialSite
            <div style={{ position: "fixed", bottom: '40%', right: this.props.displaySocialSite, zIndex: '2', }} className="socialSite">
                <div id="social-site">
                    <a href={this.state.hrefFacebook} target="_blank">
                        {/* <FacebookLoginButton size={this.state.btnSize}  onClick={() => alert("Hello")} >
                            <span>Facebook</span>
                        </FacebookLoginButton> */}
                        <FacebookLoginButton size={this.state.btnSize} >
                            <span>Facebook</span>
                        </FacebookLoginButton>
                    </a>

                </div>
                <div id="social-site">
                    <a href={this.state.hrefGoogle} target="_blank">
                        <GoogleLoginButton size={this.state.btnSize} id="social-site">
                            <span>Google+</span>
                        </GoogleLoginButton>
                    </a>
                </div>
                <div id="social-site">
                    <a href={this.state.hreTwitter} target="_blank">
                        <TwitterLoginButton size={this.state.btnSize} id="social-site" >
                            <span>Twitter</span>
                        </TwitterLoginButton>
                    </a>
                </div>
                <div id="social-site">
                    <a href={this.state.hrefInstagram} target="_blank">
                        <InstagramLoginButton size={this.state.btnSize} id="social-site" >
                            <span>Instagram</span>
                        </InstagramLoginButton>
                    </a>
                </div>
            </div>
        );
    }
}

class ScrollButton extends Component {
    constructor() {
        super();
        this.state = {
            intervalId: 0
        };
    }

    scrollStep() {
        if (window.pageYOffset === 0) {
            clearInterval(this.state.intervalId);
        }
        window.scroll(0, window.pageYOffset - this.props.scrollStepInPx);
    }

    scrollToTop() {
        let intervalId = setInterval(this.scrollStep.bind(this), this.props.delayInMs);
        this.setState({ intervalId: intervalId });
    }

    render() {
        return (
            <div className="_bottomToTop">
                <div className="_ToTop" onClick={() => { this.scrollToTop(); }}>
                    <img id="arrow-to-top" src={require('../../../assest/arrowIcon/doubleGoToTop.png')}></img>
                </div>
            </div>
        );
    }
}