<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Name: {{$names}}</h1>
    <br/>
    <h3>Email: {{$emails}}</h3>
    <br/>
    <h3>Phone Number: {{$phoneNumbers}}</h3>
    <br/>
    <h3>Project Type: {{$projectTypes}}</h3>
    <br/>
    <h2>Description of Project:</h2>
    <br/>
    <h5>{{$descriptions}}</h5>

</body>
</html>