<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Name: {{$user_name}}</h1>
    <br/>
    <h3>Email: {{$user_email}}</h3>
    <br/>
    <h2>Message from {{$user_name}}:</h2>
    <br/>
    <h5>{{$user_message}}</h5>

</body>
</html>