@extends('layouts.app')

@section('content')
    <h1>Contents</h1>
    <a class="mr-auto btn btn-outline-light" href="{{route('create')}}">
        Add New
    </a>
    @if(count($contents) > 0)
        @foreach($contents as $content)
            <div class="well">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        {{--<img style="width:100%" src="/storage/cover_images/{{$post->cover_image}}">--}}
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <h3><a href="/content/{{$content->id}}">{{$content->name}}</a></h3>
                        <small>Written on {{$content->created_at}} by</small>
                    </div>
                </div>
            </div>
        @endforeach
        {{$contents->links()}}
    @else
        <p>No content found</p>
    @endif
@endsection