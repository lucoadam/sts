<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/p','HomeController@send');
Route::post('/send-inquiry','HomeController@sendinquiry');

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');
Route::prefix('content')->group(function () {
    Route::get('/',function(){
        $contents= App\Content::orderBy('created_at','desc')->paginate(10);
        return view('admin.content.show',compact('contents'));
    })->name('contentManagement');
    Route::get('/{id}/edit',function($id){
        return $id;
    })->name('c');
    Route::get('/create',function(){
        return view('admin.content.create');
    })->name('create');
});