<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('stsdetail','Api\stsdetailController');
Route::resource('navbarmenu','Api\navbarController');
Route::resource('service','Api\serviceController');
Route::resource('feature','Api\featureController');
Route::resource('subnavbar','Api\subnavbarController');
Route::resource('content','Api\contentController');
Route::resource('image','Api\imageController');
Route::resource('interviewstaff','Api\interviewstaffController');
