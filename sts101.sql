-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: sts
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abouts`
--

DROP TABLE IF EXISTS `abouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sub_about` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `has_sub_menu` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abouts`
--

LOCK TABLES `abouts` WRITE;
/*!40000 ALTER TABLE `abouts` DISABLE KEYS */;
INSERT INTO `abouts` VALUES (1,'','',0,NULL,NULL);
/*!40000 ALTER TABLE `abouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `navbar_id` int(11) DEFAULT NULL,
  `subnavbar_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
INSERT INTO `contents` VALUES (1,'EMS','Education Management System is unique solution developed for educational institutes to manage the functions like Content Management System, Student Management System, Online Examination, Library, Video Conferencing, Virtual Classroom, clickers, Kiosk etc. It includes processing, analyzing, and reporting of educational information including   teacher, students, staff, and payroll library management etc.',NULL,1,1,'2019-01-18 00:56:39','2019-01-18 00:57:33'),(2,'Our mission is to optimize the ideas through innovation','Sustinable Technological Solution is the software development and information technology (IT) service support center for STS Analytics. We’re a dedicated team of software and IT professionals who provide technical support to businesses. At STS, we help our internal clients focus on their business solutions while we focus on the technology.',NULL,2,1,'2019-01-18 02:32:53','2019-01-18 02:32:53');
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,'image1','img/jpeg/1.jpg',1,'2019-01-18 01:02:59','2019-01-18 01:02:59');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interviewstaffs`
--

DROP TABLE IF EXISTS `interviewstaffs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interviewstaffs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interviewstaffs`
--

LOCK TABLES `interviewstaffs` WRITE;
/*!40000 ALTER TABLE `interviewstaffs` DISABLE KEYS */;
/*!40000 ALTER TABLE `interviewstaffs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(4,'2019_01_07_074825_create_stsdetails_table',2),(6,'2019_01_08_054028_create_navbars_table',3),(7,'2019_01_09_102111_create_services_table',4),(8,'2019_01_09_110244_create_abouts_table',5),(10,'2019_01_15_052046_create_subnavbars_table',6),(11,'2019_01_16_073036_create_ourfeatures_table',6),(12,'2019_01_16_174959_create_interviewstaffs_table',7),(13,'2019_01_18_061635_create_contents_table',7),(14,'2019_01_18_061959_create_images_table',7);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `navbars`
--

DROP TABLE IF EXISTS `navbars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `navbars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `navbar_menu_item` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `navbars`
--

LOCK TABLES `navbars` WRITE;
/*!40000 ALTER TABLE `navbars` DISABLE KEYS */;
INSERT INTO `navbars` VALUES (1,'services',1,'2019-01-08 06:04:29','2019-01-08 06:04:32'),(2,'about',2,'2019-01-08 06:05:00','2019-01-08 06:05:03'),(3,'career',3,'2019-01-08 06:05:13','2019-01-08 06:05:17'),(4,'contact',4,'2019-01-08 06:10:28','2019-01-08 06:10:30');
/*!40000 ALTER TABLE `navbars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ourfeatures`
--

DROP TABLE IF EXISTS `ourfeatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ourfeatures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ourfeatures`
--

LOCK TABLES `ourfeatures` WRITE;
/*!40000 ALTER TABLE `ourfeatures` DISABLE KEYS */;
INSERT INTO `ourfeatures` VALUES (1,'Inventive Use of Technology','Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.','feature/inventionuseoftechnology',NULL,'2019-01-16 02:59:33','2019-01-16 02:59:33'),(2,'The Complete Digital Solution','Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.','feature/completedigitalsolution','../../../assest/image/mobileApplication.jpg','2019-01-16 03:00:30','2019-01-16 03:00:30');
/*!40000 ALTER TABLE `ourfeatures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sub_services` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'Education Management System','Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.Our company ','services/ems','2019-01-09 10:31:16','2019-01-09 10:31:26'),(2,'Customer Relationship Management','Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.Our company ','services/cmr','2019-01-09 10:33:00','2019-01-09 10:33:03'),(3,'Mobile Application Development','Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.Our company ','services/mobile','2019-01-09 10:34:22','2019-01-09 10:34:24'),(4,'Web Application Development','Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.Our company ','services/web','2019-01-09 10:35:22','2019-01-09 10:35:24'),(5,'Data Entry','Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.Our company ','services/dataentry','2019-01-09 10:36:01','2019-01-09 10:36:04'),(6,'Quality Assurance And Testing','Our company boast of a team of dedicated App developers who help to provide App solutions and services that suits various business requirements and therefore, ensure good results.Our company ','services/qualitytesting','2019-01-09 10:36:49','2019-01-09 10:36:52');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stsdetails`
--

DROP TABLE IF EXISTS `stsdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stsdetails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stsdetails`
--

LOCK TABLES `stsdetails` WRITE;
/*!40000 ALTER TABLE `stsdetails` DISABLE KEYS */;
INSERT INTO `stsdetails` VALUES (1,'phone','01-6633004','2019-01-07 08:14:18','2019-01-07 08:14:14'),(2,'email','info@stsolutions.com.np','2019-01-07 08:14:22','2019-01-07 08:14:25'),(3,'location','Kausaltar, Bhaktapur','2019-01-07 08:14:49','2019-01-07 08:14:52');
/*!40000 ALTER TABLE `stsdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subnavbars`
--

DROP TABLE IF EXISTS `subnavbars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subnavbars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `navbar_id` int(11) NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subnavbars`
--

LOCK TABLES `subnavbars` WRITE;
/*!40000 ALTER TABLE `subnavbars` DISABLE KEYS */;
INSERT INTO `subnavbars` VALUES (1,'2019-01-17 04:36:51','2019-01-18 03:49:15','Our Mission',2,'/about/'),(2,'2019-01-17 04:37:17','2019-01-18 03:49:37','Our Office',2,'/about/'),(3,'2019-01-17 04:38:21','2019-01-18 03:50:45','Education Management System',1,'/services/3'),(4,'2019-01-17 04:40:48','2019-01-18 03:51:25','Mobile Application Development',1,'/services/4'),(5,'2019-01-17 04:42:23','2019-01-18 03:51:49','Web Application Development',1,'/services/5'),(6,'2019-01-17 04:42:50','2019-01-18 03:52:07','Data Entry',1,'/services/6'),(7,'2019-01-17 04:43:40','2019-01-18 03:52:41','Quality Assurance And Testing',1,'/services/7');
/*!40000 ALTER TABLE `subnavbars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-18 16:42:47
