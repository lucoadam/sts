<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class navbar extends Model
{
    //
    public function subnavbar(){
        return $this->hasMany('App\subnavbar');
    }
    public function content(){
        return $this->hasMany('App\Content');
    }
}
