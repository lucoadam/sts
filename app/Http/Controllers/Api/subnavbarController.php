<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\subnavbar;
class subnavbarController extends Controller
{
    //
    public function index(){
        $subnavbars = subnavbar::all();
        return response()->json($subnavbars);
    }
    public function show($id){
        $subnavbar = subnavbar::findOrFail($id);
        return response()->json($subnavbar);
    }
}
