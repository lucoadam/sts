<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subnavbar extends Model
{
    //
    public function navbar(){
        return $this->belongsTo('App\navbar');
    }
    public function content(){
        return $this->hasMany('App\Content');
    }
}
