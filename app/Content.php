<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    //
    public function navbar(){
        return $this->belongsTo('App\navbar');
    }
    public function image(){
        return $this->hasMany('App\Image');
    }
    public function subnavbar(){
        return $this->belongsTo('App\subnavbar');
    }
}
